package services;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DbService {

    //todo fix query regex
    public static final String CREATE_TABLE = "create\\s+table\\s+(\\w+)";
    public static final String INSERT_ROW = "((?<=(INSERT\\sINTO\\s))[\\w\\d_]+(?=\\s+))|((?<=\\()([\\w?\\d?_,]*|[(?:^|\\s)'([^']*?)'(?:$|\\s),]*)+(?=\\)))";
    public static final String DELETE_ROW = "";
    public static final String UPDATE_ROW = "";
    public static final String SELECT_TABLE = "";


    public static boolean isValidQuery(String query) {
        return query.matches(CREATE_TABLE)
                || query.matches(INSERT_ROW)
                || query.matches(DELETE_ROW)
                || query.matches(UPDATE_ROW)
                || query.matches(SELECT_TABLE);
    }

    public static void create(String query, String fields) {
        Pattern re = Pattern.compile(CREATE_TABLE, Pattern.CASE_INSENSITIVE);
        Matcher m = re.matcher(query);

        String table = null;
        while (m.find()) {
            table = m.group(1);
        }

        FileService.create(table, fields);
    }

    public static void select(String query) {
        Pattern re = Pattern.compile(SELECT_TABLE, Pattern.CASE_INSENSITIVE);

        Matcher m = re.matcher(query);

        String table = null;
        String fields = null;
        String conditions = null;
        while (m.find()) {
            table = m.group(1);
            fields = m.group(2);
            conditions = m.group(3);
        }
        FileService.select(table, fields, conditions);
    }

    public static void insert(String query) {
        Pattern re = Pattern.compile(INSERT_ROW, Pattern.CASE_INSENSITIVE);

        Matcher m = re.matcher(query);
        String table = null;
        String fields = null;
        String values = null;
        while (m.find()) {
            table = m.group(1);
            fields = m.group(2);
            values = m.group(3);
        }


        FileService.insert(table, fields, values);
    }

    public static void update(String query) {
        Pattern re = Pattern.compile(UPDATE_ROW, Pattern.CASE_INSENSITIVE);

        Matcher m = re.matcher(query);
        String table = null;
        String fields = null;
        String condition = null;
        while (m.find()) {
            table = m.group(1);
            fields = m.group(2);
            condition = m.group(3);
        }
        FileService.update(table, fields, condition);

    }

    public static void delete(String query) {
        Pattern re = Pattern.compile(DELETE_ROW, Pattern.CASE_INSENSITIVE);

        Matcher m = re.matcher(query);
        String table = null;
        String conditions = null;
        while (m.find()) {
            table = m.group(1);
            conditions = m.group(2);
        }


        FileService.delete(table, conditions);
    }
}