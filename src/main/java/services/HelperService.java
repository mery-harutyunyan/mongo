package services;

import org.json.JSONArray;
import org.json.JSONObject;

public class HelperService {

    public static String getFirstWordOfString(String string) {
        String arr[] = string.split(" ", 2);

        return arr[0];
    }

    public static JSONObject stringToJSONObject(String string, String key) {
        String arrayStr[] = string.split(",");

        JSONObject json = new JSONObject();
        JSONArray array = new JSONArray();
        JSONObject item = new JSONObject();
        for (String s : arrayStr) {
            item.put(s, "");
        }
        array.put(item);
        json.put(key, array);
        return json;
    }

    public static boolean JSONArrayHas(JSONArray jsonArray, String field) {
        return jsonArray.toString().contains("\"" + field + "\":\"");
    }
}
