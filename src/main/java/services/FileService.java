package services;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.*;
import java.nio.file.FileSystems;
import java.util.UUID;

public class FileService {
    public static final String DB_PATH = "db";
    public static final String DS = FileSystems.getDefault().getSeparator();

    public static JSONObject getTableInfo(String table) {
        File initialFile = new File(DB_PATH + DS + table + ".json");
        JSONTokener tokener = new JSONTokener("");
        try {
            InputStream is = new FileInputStream(initialFile);
            tokener = new JSONTokener(is);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return new JSONObject(tokener);
    }

    public static void create(String path, String data) {
        File tempFile = new File(DB_PATH + DS + path + ".json");

        if (tempFile.exists()) {
            System.out.println("Table '" + path + "' already exists");
            return;
        }

        try {
            tempFile.createNewFile();
            JSONObject tableStructure = HelperService.stringToJSONObject(data, "structure");
            FileService.writeJson(path, tableStructure);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void insert(String table, String fields, String values) {
        JSONObject tableInfo = getTableInfo(table);
        String fieldsArray[] = fields.split(",");
        String valuesArray[] = values.split(",");

        JSONArray structure = tableInfo.getJSONArray("structure");
        UUID uuid = UUID.randomUUID();

        for (String s : fieldsArray) {
            if (!HelperService.JSONArrayHas(structure, s)) {
                System.out.println(s + " column does not exists in" + table + " table");
            }
        }

        JSONArray data = new JSONArray();
        if (tableInfo.has("data")) {
            data = tableInfo.getJSONArray("data");
        }

        JSONObject item = new JSONObject();
        item.put("_id", uuid);
        for (int i = 0; i < fieldsArray.length; i++) {
            item.put(fieldsArray[i], valuesArray[i]);
        }
        data.put(item);

        if (!tableInfo.has("data")) {
            tableInfo.put("data", data);
        }

        writeJson(table, tableInfo);
    }

    public static JSONArray find(String table, String fields, String condition) {
        JSONArray filtered = new JSONArray();
        JSONObject tableInfo = getTableInfo(table);
        JSONArray tableData = tableInfo.getJSONArray("data");
        String[] conditionArray = condition.split("=");

        for (int i = 0; i < tableData.length(); i++) {
            JSONObject jsonData = tableData.getJSONObject(i);
            if (jsonData.getString(conditionArray[0]).equals(conditionArray[1])) {
                filtered.put(jsonData);
            }
        }

        return filtered;
    }

    public static void select(String table, String field, String conditions) {
        JSONArray data = find(table, field, conditions);
        System.out.println(data.toString());
    }

    public static void update(String table, String updates, String conditions) {
        JSONObject tableInfo = getTableInfo(table);
        JSONArray tableData = tableInfo.getJSONArray("data");
        String[] conditionArray = conditions.split("=");
        String[] updatesArray = updates.split(",");

        for (int i = 0; i < tableData.length(); i++) {
            JSONObject jsonData = tableData.getJSONObject(i);
            if (jsonData.getString(conditionArray[0]).equals(conditionArray[1])) {
                for (String data : updatesArray) {
                    String[] updateData = data.split("=");
                    jsonData.put(updateData[0], updateData[1]);
                }
            }
        }

        tableInfo.put("data", tableData);
        writeJson(table, tableInfo);
    }

    public static void delete(String table, String conditions) {
        JSONObject tableInfo = getTableInfo(table);
        JSONArray tableData = tableInfo.getJSONArray("data");
        String[] conditionArray = conditions.split("=");

        for (int i = 0; i < tableData.length(); i++) {
            JSONObject jsonData = tableData.getJSONObject(i);
            if (jsonData.getString(conditionArray[0]).equals(conditionArray[1])) {
                tableData.remove(i);
            }
        }

        tableInfo.put("data", tableData);
        writeJson(table, tableInfo);
    }

    public static void writeJson(String table, JSONObject obj) {
        FileWriter file = null;
        try {

            file = new FileWriter(DB_PATH + DS + table + ".json");
            file.write(obj.toString());

        } catch (IOException e) {
            e.printStackTrace();

        } finally {

            try {
                assert file != null;
                file.flush();
                file.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}