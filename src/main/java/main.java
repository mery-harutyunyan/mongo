import services.DbService;
import services.HelperService;

import java.io.IOException;
import java.util.Scanner;

public class main {

    public static final String CREATE = "create";
    public static final String INSERT = "insert";
    public static final String UPDATE = "update";
    public static final String DELETE = "delete";
    public static final String SELECT = "select";

    public static void main(String[] args) throws IOException {

        while (true) {
            System.out.println("Enter valid query");

            Scanner scanner = new Scanner(System.in);
            String query = scanner.nextLine();

            if (query.equals("q")) {
                break;
            }

            boolean isValidQuery = DbService.isValidQuery(query);

//            if (!isValidQuery) {
//                System.out.println("Not valid query");
//                continue;
//            }

            String queryType = HelperService.getFirstWordOfString(query).toLowerCase();

            switch (queryType) {
                case CREATE:
                    String fields = "";
                    while (fields == "") {
                        System.out.println("Insert fields separated with comma");
                        fields = scanner.nextLine();
                    }

                    DbService.create(query, fields);
                    break;
                case INSERT:
                    DbService.insert(query);
                    break;
                case UPDATE:
                    DbService.update(query);
                    break;
                case SELECT:
                    DbService.select(query);
                    break;
                case DELETE:
                    DbService.delete(query);
                    break;
            }
        }
    }
}
